Source: golang-github-hashicorp-memberlist
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Tianon Gravi <tianon@debian.org>,
           Tim Potter <tpot@hpe.com>,
           Dmitry Smirnov <onlyjob@debian.org>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-dns-dev,
               golang-github-armon-go-metrics-dev,
               golang-github-google-btree-dev,
               golang-github-hashicorp-go-msgpack-dev (>= 0.5.5~),
               golang-github-hashicorp-go-multierror-dev,
               golang-github-hashicorp-go-sockaddr-dev,
               golang-github-miekg-dns-dev (>= 1.1.26~),
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-memberlist
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-memberlist.git
Homepage: https://github.com/hashicorp/memberlist
XS-Go-Import-Path: github.com/hashicorp/memberlist
Rules-Requires-Root: no

Package: golang-github-hashicorp-memberlist-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-armon-go-metrics-dev,
         golang-github-google-btree-dev,
         golang-github-hashicorp-go-msgpack-dev (>= 0.5.5~),
         golang-github-hashicorp-go-multierror-dev,
         golang-github-hashicorp-go-sockaddr-dev,
         golang-github-miekg-dns-dev (>= 1.1.26~),
Description: Golang package for gossip based membership and failure detection
 memberlist is a Go library that manages cluster membership and member failure
 detection using a gossip based protocol.
 .
 The use cases for such a library are far-reaching: all distributed systems
 require membership, and memberlist is a re-usable solution to managing cluster
 membership and node failure detection.
 .
 memberlist is eventually consistent but converges quickly on average. The speed
 at which it converges can be heavily tuned via various knobs on the protocol.
 Node failures are detected and network partitions are partially tolerated by
 attempting to communicate to potentially dead nodes through multiple routes.
 This package contains the source.
